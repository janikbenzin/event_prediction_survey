# Supplementary Material for
## A Survey on Event Prediction Methods from a Systems Perspective: Bringing Together Disparate Research Areas
authored by Janik-Vasily Benzin and Stefanie Rinderle-Ma, submitted to ACM 

Read the paper: [Arxiv Technical Report](https://arxiv.org/pdf/2302.04018)

## Getting started

This supplementary material consists of the classification and assessment of 260 event prediction method articles. 
The classification is available in a raw, unprocessed format: raw_classification.csv
The assessment is available in a raw, unprocessed format: raw_assessment.csv

The processed, final assessment and classification is available at: final_assessment_classification.csv

The analysis and processing code that is used to create the figures, tables and processed formats is analysis.py. 

