from __future__ import print_function # Only Python 2.x
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
from functools import partial
import string
import warnings
import textwrap
import itertools
import bibtexparser
import subprocess


# Path to the Event Prediction Survey PDF https://dl.acm.org/doi/10.1145/3450287
ZHAO_EVENT_PREDICTION_SURVEY_PATH = ""
# Working directory
PATH = './'


def execute(cmd):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    out = []
    for stdout_line in iter(popen.stdout.readline, ""):
        out.append(stdout_line)
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)
    return out




def wrap_labels(ax, width, y=True, x=True, break_long_words=False):
    if y:
        labels = []
        for label in ax.get_yticklabels():
            text = label.get_text()
            labels.append(textwrap.fill(text, width=width,
                                        break_long_words=break_long_words))
        ax.set_yticklabels(labels, rotation=0)
    if x:
        labels = []
        for label in ax.get_xticklabels():
            text = label.get_text()
            labels.append(textwrap.fill(text, width=width,
                                        break_long_words=break_long_words))
        ax.set_xticklabels(labels, rotation=0)


plt.ioff()

warnings.simplefilter(action='ignore', category=FutureWarning)


def find_whitespace(st):
    positions = []
    for index, character in enumerate(st):
        if character in string.whitespace:
            positions.append(index)
    return positions

plt.rcParams.update({'font.size': 9})


criterias = ["1.2", "1.3", "1.6", "1.7", "2", "3.1", "3.2", "9.1", "9.3", "10", "5", "4", "15", "12", "14"]
criterias_mapping = {"1.2": "Prob. Prediction",
                     "1.3": "Suffix Prediction",
                     "1.6": "Lifecycle Prediction",
                     "1.7": "Uncertainty Quant.",
                     "2": "RCA Support",
                     "3.1": "Output Visual.",
                     "3.2": "RCA Visual.",
                     "9.1": "Precise RCA",
                     "9.3": "Joint RCs",
                     "10": "Explainability",
                     "5": "Prob. Quality",
                     "4": "Online Concept.",
                     "12": "Matching",
                     "14": "Data Quality",
                     "15": "Multiple Sources"}
criterias = [criterias_mapping[c] for c in criterias]


def extract_combination(row):
    if "Combination" in row["Family of Model"] \
            :  # or "/" in row["Family of Model"]:
        return "Com"
    else:
        return row["Family of Model"]


def extract_joint(row):
    if "and" in row["Prediction Output"]:
        return "Joint"
    else:
        return row["Prediction Output"]


def extract_joint_rep(row):
    if "and" in row["Output Representation"]:
        return "Joint"
    else:
        return row["Output Representation"]


def aggregate_sources(rows):
    if "cite" in rows.str.cat(sep=" "):
        return_string = " ".join([f"{sr}{do}" for sr, do in zip([s for s in rows.to_list()[:int(len(rows) / 2)]],
                                                                [DOM_SYM_MAPPING[d.strip()] for d in
                                                                 rows.to_list()[int(len(rows) / 2):]])])
        return return_string
    return rows.str.cat(sep=" ")


def aggregate_criteria(rows):
    m = rows.max()
    m_c = len([1 for i, v in rows.items() if v == rows.max()])
    m_a = len(rows)
    return [m, m_c, m_c / m_a]


def format_makecell(replace, orientation, c):
    if replace:
        c = str(c)
        if "~" in c:
            c = c.replace("~", "$\\sim$")
        elif "+" in c:
            c = c.replace("+", "$+$")
        elif "-" in c:
            c = c.replace("-", "$-$")
    if "cite" in c:
        return str(c).replace(" $", "$")
    else:
        return str(c)


def format_taxonomy(c):
    if len(c) > 12 and len(c) < 24:
        ps = find_whitespace(c)
        if len(ps) > 0:
            return "\makecell[l]{" + c[0:ps[0]] + "\\\\" + c[ps[0]:] + "}"
        else:
            return "\makecell[l]{" + c[0:12] + "\\\\" + c[12:] + "}"
    elif len(c) > 24:
        ps = find_whitespace(c)
        if len(ps) > 2:
            return "\makecell[l]{" + c[0:ps[0]] + "\\\\" + c[ps[0]:ps[2]] + "\\\\" + c[ps[2]:] + "}"
        else:
            return "\makecell[l]{" + c[0:12] + "\\\\" + c[12:24] + "\\\\" + c[24:] + "}"
    else:
        return c


def get_fraction_of(val, curr_row):
    return len([1 for i, r in curr_row.items() if r == val]) / len(curr_row)


def write_latex(frame, name, replace=False, caption="", clines=None, row_align="l", col_align="c", index_align="l",
                column_format=""):
    if caption == "":
        caption = name
    with pd.option_context("max_colwidth", 1000):
        with open(f"{PATH}{name}.tex", "w") as t:
            frame.style.format().format(partial(format_makecell, replace, col_align)).format_index(
                partial(format_makecell, replace, index_align)).to_latex(buf=t,
                                                                         hrules=True,
                                                                         multirow_align=row_align,
                                                                         multicol_align=col_align,
                                                                         clines=clines,
                                                                         column_format=column_format,
                                                                         label=f"fig:{name}",
                                                                         caption=caption
                                                                         )


def replace_cell(cell):
    if cell == "+":
        return 1
    elif cell == "~":
        return 0
    elif cell == "-":
        return -1


DOM_CYBER = "Cyber Systems"
DOM_HEALTHCARE = "Healthcare"
DOM_MEDIA = "Media"
DOM_CRIME = "Crime"
DOM_NATURAL_DISASTER = "Natural Disasters"
DOM_TRANSPORTATION = "Transportation"
DOM_ENG = "Engineering Systems"
DOM_POLITICAL = "Political Events"
DOM_BUSINESS = "Business"
DOM_OTHER = "Other"

DOM_SYM_MAPPING = {
    DOM_CYBER: "$^I$",
    DOM_HEALTHCARE: "$^H$",
    DOM_MEDIA: "$^M$",
    DOM_CRIME: "$^C$",
    DOM_NATURAL_DISASTER: "$^N$",
    DOM_TRANSPORTATION: "$^T$",
    DOM_ENG: "$^E$",
    DOM_POLITICAL: "$^P$",
    DOM_BUSINESS: "$^B$",
    DOM_OTHER: "$^O$"
}


def replace_titles(cell):
    if isinstance(cell, str):
        cell = cell.replace("Time", "Tim")
        cell = cell.replace("Location", "Loc")
        cell = cell.replace("Semantic", "Sem")

        cell = cell.replace("Discrete state system", "DSS")
        cell = cell.replace("Continuous state system", "CSS")

        cell = cell.replace("Discrete", "Dis")
        cell = cell.replace("Continuous", "Con")
        cell = cell.replace("Occurrence", "Occ")
        cell = cell.replace("Raster-based", "Ras")
        cell = cell.replace("Point-based", "Poi")
        cell = cell.replace("Categorical", "Cat")
        cell = cell.replace("Aggregate", "Agg")

        cell = cell.replace("Inferential", "Inf")
        cell = cell.replace("Hybrid", "Hyb")
        cell = cell.replace("Generative", "Gen")

        cell = cell.replace("Supervised", "Sup")
        cell = cell.replace("Unsupervised", "Uns")
        cell = cell.replace("Reinforcement", "Rei")
        cell = cell.replace("Analytical", "Ana")
        cell = cell.replace("Semi-supervised", "Sem")
        cell = cell.replace("Transfer", "Tra")

        cell = cell.replace("Neural network", "NN")
        cell = cell.replace("Bayesian-based", "Bay")
        cell = cell.replace("Combination", "Com")
        cell = cell.replace("Markov-based", "Mar")
        cell = cell.replace("Distance-based", "Dis")
        cell = cell.replace("Association-based", "Ass")
        cell = cell.replace("Regression", "Reg")
        cell = cell.replace("Tensor-based", "Ten")

        cell = cell.replace("Direct", "Dir")
        cell = cell.replace("Indirect", "Ind")
        cell = cell.replace("(no checking)", "")


        cell = cell.replace("Tim and Loc and Sem", "Tim, Loc and Sem")
        cell = cell.replace("Occ and Ras and Cat", "Occ, Ras and Cat")
        cell = cell.replace("Dis and Ras and Cat", "Dis, Ras and Cat")
        cell = cell.replace("Dis and Ras and Agg", "Dis, Ras and Agg")

        # Domain
        cell = cell.replace("IT Infrastructure", DOM_CYBER)
        cell = cell.replace("Mobile Infrastructure", DOM_CYBER)
        cell = cell.replace("Mobile infrastructure", DOM_CYBER)
        cell = cell.replace("Mobile user", DOM_BUSINESS)
        cell = cell.replace("?", DOM_OTHER)
        cell = cell.replace("Network Infrastructure", DOM_CYBER)
        cell = cell.replace("Network infrastructure", DOM_CYBER)
        cell = cell.replace("IT Infrastructure", DOM_CYBER)
        cell = cell.replace("IT Infrastructure", DOM_CYBER)
        cell = cell.replace("Advertising", DOM_BUSINESS)
        cell = cell.replace("Aviation", DOM_ENG)
        cell = cell.replace("Business failures", DOM_BUSINESS)
        cell = cell.replace("Business processes", DOM_BUSINESS)
        cell = cell.replace("CPS / Manufacturing", DOM_ENG)
        cell = cell.replace("CPS", DOM_ENG)
        cell = cell.replace("Criminality", DOM_CRIME)
        cell = cell.replace("Crowd", DOM_TRANSPORTATION)
        cell = cell.replace("Customer behavior", DOM_BUSINESS)
        cell = cell.replace("Disease", DOM_HEALTHCARE)
        cell = cell.replace("Electrical Infrastructure", DOM_ENG)
        cell = cell.replace("Engineering Systems", DOM_ENG)
        cell = cell.replace("Engineering systems", DOM_ENG)
        cell = cell.replace("Environment", DOM_NATURAL_DISASTER)
        cell = cell.replace("Finance", DOM_BUSINESS)
        cell = cell.replace("Healthcare", DOM_HEALTHCARE)
        cell = cell.replace("Internet behavior", DOM_BUSINESS)
        cell = cell.replace("IT hardware", DOM_CYBER)
        cell = cell.replace("IT Service", DOM_CYBER)
        cell = cell.replace("IT service", DOM_CYBER)
        cell = cell.replace("Logistics", DOM_TRANSPORTATION)
        cell = cell.replace("Natural disaster", DOM_NATURAL_DISASTER)
        cell = cell.replace("Natural language instructions", DOM_MEDIA)
        cell = cell.replace("News", DOM_MEDIA)
        cell = cell.replace("NLP", DOM_MEDIA)
        cell = cell.replace("Recommender systems", DOM_BUSINESS)
        cell = cell.replace("Smart home", DOM_ENG)
        cell = cell.replace("Social Media", DOM_ENG)
        cell = cell.replace("Social", DOM_POLITICAL)
        cell = cell.replace("Software", DOM_CYBER)
        cell = cell.replace("Theoretical stochastic process", DOM_CRIME)
        cell = cell.replace("Traffic", DOM_TRANSPORTATION)
        cell = cell.replace("Video", DOM_MEDIA)
        cell = cell.replace("Weather", DOM_NATURAL_DISASTER)
        return cell.strip()
    else:
        return cell

def replace_box(cell):
    cell = cell.lower()
    if "black box" in cell:
        cell = "-"
    elif "white box" in cell:
        cell = "+"
    elif "both" in cell:
        cell = "~"
    else:
        cell = "-"
    return cell


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    df_ass = pd.read_csv(PATH + 'raw_assessment.csv')
    # Requirements that were removed during one of the 260 analysis cycles, but still remained in the raw data
    df_ass.drop(labels=["1.1", "1.4", "1.5", "6", "7", "9.2", "11", "13", "Unnamed: 32"], axis=1, inplace=True)
    ass_records = df_ass.to_dict("records")
    df = pd.read_csv(PATH + 'raw_classification.csv', sep="&")
    df = df[["Publication", "Class"]]
    df["Intermediate"] = df["Class"].str.split(",")
    df["Publication"] = df["Publication"].str.strip()
    records = df.to_dict("records")
    excluded = []
    excluded_first = []
    final = []
    for r in ass_records[:-1]:
        if type(r["Prediction?"]) is str:
            excluded_first.append(r)
            continue
        else:
            class_record = df.loc[df['Publication'] == r["Sources"].strip()]
            record = class_record["Intermediate"].tolist()
            if len(record) >= 1:
                record = record[0]
            else:
                excluded.append(r)
                print(class_record)
                continue
            if type(record) is not list:
                excluded.append(r)
                print(class_record)
                continue
            if len(record) < 6:
                excluded.append(r)
                print(class_record)
                continue
            else:
                r["Prediction Output"] = record[0].strip()
                r["Output Representation"] = record[1].strip()
                r["Basic Model"] = record[2].strip()
                r["Family of Model"] = record[3].strip()
                r["Learning Setup"] = record[4].strip()
                final.append(r)
    del df
    del class_record
    del ass_records
    del df_ass
    del records
    df_final = pd.DataFrame(final).rename(
        columns=criterias_mapping
    )
    classes = ["Prediction Output", "Output Representation", "Basic Model", "Basic Technique"]
    df_final = df_final.applymap(replace_titles)
    out_type = ["Tim", "Loc", "Sem", "Tim and Loc", "Tim and Sem",
                "Tim, Loc and Sem"]
    out_cat_type = pd.CategoricalDtype(categories=out_type, ordered=True)
    df_final["Prediction Output"] = df_final["Prediction Output"].astype(out_cat_type)
    # Remove duplicate
    df_final = df_final.drop(
        [i for i, v in df_final['Sources'].str.find('\\cite{taymouri_predictive_2020}').items() if v == 0][0])

    tim_type = ["Occ", "Dis", "Con"]
    loc_type = ["Ras", "Poi"]
    sem_type = ["Cat", "Agg"]
    rep_type = tim_type + loc_type + sem_type
    for element in itertools.product(tim_type, loc_type):
        rep_type.append(element[0] + " and " + element[1])
    for element in itertools.product(tim_type, sem_type):
        rep_type.append(element[0] + " and " + element[1])
    for element in itertools.product(tim_type, loc_type, sem_type):
        rep_type.append(element[0] + ", " + element[1] + " and " + element[2])
    rep_cat_type = pd.CategoricalDtype(categories=rep_type, ordered=True)
    df_final["Output Representation"] = df_final["Output Representation"].astype(rep_cat_type)

    bas_type = ["Inf", "Hyb", "Gen"]
    bas_cat_type = pd.CategoricalDtype(categories=bas_type, ordered=True)
    df_final["Basic Model"] = df_final["Basic Model"].astype(bas_cat_type)

    bast_type = ["Dir", "Ind"]
    bast_cat_type = pd.CategoricalDtype(categories=bast_type, ordered=True)
    df_final["Basic Technique"] = df_final["Basic Technique"].astype(bast_cat_type)
    df_final["Explainability"] = [replace_box(cell) for cell in df_final["Explainability"].to_list()]

    evals = ["-", "~", "+"]
    cat_type = pd.CategoricalDtype(categories=evals, ordered=True)
    for c in criterias:
        df_final[c] = df_final[c].astype(cat_type)
    df_final = df_final.sort_values(by=classes)
    prediction_outputs = df_final["Prediction Output"].unique()
    df_melted = pd.melt(df_final, id_vars=classes)
    df_sources = df_melted[(df_melted["variable"] == "Sources") | (df_melted["variable"] == "Domain1")]
    df_classgrouped = df_sources.groupby(classes, sort=False)
    df_sources = df_classgrouped.aggregate(aggregate_sources).rename(columns={"value": "Sources"})
    df_sources = df_sources.dropna()
    secs = {}
    singles = ["Tim", "Loc", "Sem"]
    joints = [s for s in prediction_outputs if s not in singles]
    for sec in singles:
        to_add = df_sources.loc[sec]
        col = [sec for i in range(len(to_add))]
        to_add["Prediction Output"] = col
        secs[sec] = to_add
    for sec in joints:
        to_add = df_sources.loc[sec]
        col = [sec for i in range(len(to_add))]
        to_add["Prediction Output"] = [v for i, v in
                                       df_sources.reset_index()[df_sources.reset_index()["Prediction Output"] == sec][
                                           "Prediction Output"].items()]
        if "Joint" in secs:
            secs["Joint"] = pd.concat([secs["Joint"], to_add])
        else:
            secs["Joint"] = to_add

    for k in secs.keys():
        if k != "Joint":
            secs[k] = secs[k].reset_index().set_index([c for c in classes if c != "Prediction Output"])[
                ["Sources"]]
        else:
            secs[k] = secs[k].reset_index().set_index(classes)[
                ["Sources"]]
        sec_column_string = 'p{1cm}p{1cm}p{1cm}p{10.5cm}'
        with pd.option_context("max_colwidth", 1000):
            with open(f"{PATH}{k}.tex", "w") as t:
                secs[k].style.format().format(partial(format_makecell, False, "c")).format_index(
                    partial(format_makecell, False, "l")).to_latex(buf=t, hrules=True, multirow_align="l",
                                                                   multicol_align="c", clines="all;index",
                                                                   column_format=sec_column_string,
                                                                   label=k,
                                                                   caption=k)
    groups = ["Prediction Output", "Output Representation", "Basic Model", "Basic Technique"]
    df_ass = df_final[groups + criterias]
    results = {k: {} for k in singles + ["Joint"]}
    for sec in singles:
        results[sec]["max"] = df_ass.groupby(groups, sort=False).max().dropna().loc[sec]
        results[sec]["maxf"] = df_ass.groupby(groups).aggregate(aggregate_criteria).dropna().loc[sec]
        results[sec]["min"] = df_ass.groupby(groups).min().dropna().loc[sec]
        results[sec]["median"] = df_ass.groupby(groups).min().dropna().loc[sec]
    df_j = []
    for j in joints:
        df_j.append(df_ass[df_ass["Prediction Output"] == j])
    df_j = pd.concat(df_j)
    results["Joint"]["max"] = df_j.groupby(groups).max().dropna()
    results["Joint"]["maxf"] = df_j.groupby(groups).aggregate(aggregate_criteria).dropna()
    results["Joint"]["min"] = df_j.groupby(groups).min().dropna()
    results["Joint"]["median"] = df_j.groupby(groups).min().dropna()
    results["All"] = {}
    results["All"]["AllColumn"] = df_ass.max()
    for c in criterias:
        curr_row = df_ass[c]
        results["All"][c] = {
            evals[0]: get_fraction_of(evals[0], curr_row),
            evals[1]: get_fraction_of(evals[1], curr_row),
            evals[2]: get_fraction_of(evals[2], curr_row)}

    df_ass_sources = df_final[groups + criterias + ["Sources"]]
    source_stat = [r.value_counts() for i, r in df_ass_sources.iterrows()]
    results_sources = {}
    for e in evals:
        results_sources[e] = [

        ]

    for t in source_stat:
        t = t.to_dict()
        for e in evals:
            if e in t:
                results_sources[e].append(t[e])
            else:
                results_sources[e].append(0)
    m = 0
    for t in source_stat:
        t = t.to_dict()
        for v in t.keys():
            if "cite" in v:
                s = v
                break
        results_sources[s] = []
        for e in evals:

            if e in t:
                frac = t[e] / len(criterias)
                if frac > m and e == "+":
                    m = frac
                    max_source = s

                results_sources[s].append(t[e] / len(criterias))
            else:
                results_sources[s].append(0)
    d = pd.DataFrame(results_sources, columns=evals)
    color_plus = '#155195'
    color_tilde = "#0491d1"
    color_minus = "#82d5f6"
    f, ax = plt.subplots(
        figsize=(2.5, 5)
    )
    sns.boxplot(d["+"], color=color_plus).set(
        xlabel = "+ assessment result per paper",
        ylabel = "# of criteria",
        xticklabels=[]
    )

    plt.savefig(f"{PATH}boxplot.png", bbox_inches='tight')
    plt.close()
    results_sources["Summary"] = d.describe()
    write_latex(results["All"]["AllColumn"].to_frame().transpose(), "maxallcolumns", replace=True,
                column_format="lcccccccccccccc")
    simple_groups = ["Basic Model", "Basic Technique"]
    results["JointSimple"] = {}
    results["JointSimple"]["max"] = df_j[[c for c in df_j.columns if c in simple_groups or c in criterias]].groupby(
        simple_groups).max().dropna()
    for k in results:
        if k != "All":
            if k == "Joint":
                f, ax = plt.subplots(
                    figsize=(10, 15)
                )
                ylbl = "-".join(classes)
            elif k == "JointSimple":
                f, ax = plt.subplots(
                    figsize=(10, 5)
                )
                ylbl = "-".join(simple_groups)
            elif k == "Tim":
                f, ax = plt.subplots(
                    figsize=(10, 5)
                )
                ylbl = "-".join([c for c in classes if c != "Prediction Output"])
            else:
                f, ax = plt.subplots(
                    figsize=(10, 5)
                )
                ylbl = "-".join([c for c in classes if c != "Prediction Output"])


            cmap = sns.color_palette([color_minus, color_tilde, color_plus])
            s = sns.heatmap(results[k]["max"].applymap(replace_cell).apply(lambda col: pd.to_numeric(col.astype(str))),
                            cmap=cmap, linecolor="black", linewidths=0.5)
            for _, spine in s.spines.items():
                spine.set_visible(True)
            ax.set_xlabel('Assessment Criteria')
            ax.axvline(x=11, linestyle="--", color="#fcd006", linewidth=2.5)
            ax.axvline(x=7, linestyle="--", color="#fcd006", linewidth=2.5)
            h = 0.92
            if k != "JointSimple":
                ax.annotate(f'Functional Requirements',
                            xy=(0.21, h), xycoords='figure fraction')
                ax.annotate(f'Attributes',
                            xy=(0.51, h), xycoords='figure fraction')
                ax.annotate(f'Constraints',
                            xy=(0.69, h), xycoords='figure fraction')
            else:
                ax.annotate(f'Functional Requirements',
                            xy=(0.19, h), xycoords='figure fraction')
                ax.annotate(f'Attributes',
                            xy=(0.495, h), xycoords='figure fraction')
                ax.annotate(f'Constraints',
                            xy=(0.675, h), xycoords='figure fraction')


            ax.set_ylabel('Event Prediction Classes\n' + ylbl)
            wrap_labels(ax, 15, x=False)
            wrap_labels(ax, 10, y=False)
            plt.xticks(rotation=45)
            colorbar = ax.collections[0].colorbar
            r = colorbar.vmax - colorbar.vmin
            colorbar.set_ticks([colorbar.vmin + r / 3 * (0.5 + i) for i in range(3)])
            colorbar.set_ticklabels(evals)
            prop = {'size': 50}
            plt.tight_layout()
            plt.savefig(f"{PATH}{k.lower()}.png", bbox_inches='tight')
            plt.close(f)


    sum_index = [k for k in results["All"] if k != "AllColumn"]
    minus = [results["All"][i]["-"] for i in sum_index]
    plus = [results["All"][i]["+"] for i in sum_index]
    tilde = [results["All"][i]["~"] + results["All"][i]["+"] for i in sum_index]
    sum_data = pd.DataFrame(list(zip(sum_index, minus, tilde, plus, [1.0 for i in range(len(minus))])),
                            columns=["criteria"] + evals + ["sum"])
    f, ax = plt.subplots(
        figsize=(10, 4.5)
    )
    max_v = list(zip([results["All"]["AllColumn"].to_frame().transpose()[v].to_list()[0] for v in sum_index],
                     [52.5 + i * 41.95 for i in range(len(sum_index))]))
    for m, v in max_v:
        ax.annotate(f'best:\n   {m}',
                    xy=(v, 220), xycoords='figure points')
    ax.axvline(x=6.5, linestyle="--", color="black")
    ax.annotate(f'Functional Requirements',
                xy=(135, 297), xycoords='figure points')
    ax.annotate(f'Attributes',
                xy=(403, 297), xycoords='figure points')
    ax.annotate(f'Constraints',
                xy=(570, 297), xycoords='figure points')
    ax.axvline(x=10.5, linestyle="--", color="black")
    ax.axhline(y=0.2, color="black", linestyle="dotted")
    ax.annotate(f'λ',
                xy=(676, 116), xycoords='figure points')
    bar_total = sns.barplot(x="criteria", y="sum", data=sum_data, color=color_minus)
    bar_tilde = sns.barplot(x="criteria", y="~", data=sum_data, color=color_tilde)
    bar_minus = sns.barplot(x="criteria", y="+", data=sum_data, color=color_plus)
    # add legend
    top_bar = mpatches.Patch(color=color_plus, label='+')
    tilde_bar = mpatches.Patch(color=color_tilde, label='~')
    minus_bar = mpatches.Patch(color=color_minus, label='-')
    plt.legend(handles=[top_bar, tilde_bar, minus_bar], loc='lower left', bbox_to_anchor=(0.9, 0))
    ax.set_xlabel('Assessment Requirements')
    ax.set_ylabel('Overall Article Share')
    wrap_labels(ax, 10, y=False)
    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.savefig(f"{PATH}sum.png", bbox_inches='tight')
    plt.close(f)
    domains = df_final["Domain1"].value_counts()
    domain_counts = domains.to_list()
    domain_frac = pd.DataFrame(domains).rename(columns={"Domain1": "Share"}).applymap(lambda v: str(round(v / 262, 2)))
    domain_frac["Count"] = [str(i) for i in domain_counts]
    write_latex(domain_frac, "Domain", column_format="lcc")
    df_search = df_final["Area"].value_counts()
    df_search = pd.DataFrame(df_search)
    write_latex(df_search.applymap(lambda c: str(c)), "Search", column_format="lc")

    with open(f'{PATH}bib.tex') as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)

    df_final["RawSource"] = df_final["Sources"].apply(lambda v: v.strip().split("{")[1].split("}")[0])

    titles = [(bib_database.entries_dict[v]['title'].replace("{", "").replace("}", "")[:32],
               v,
               bib_database.entries_dict[v]['year']) for i, v in
              df_final["RawSource"].items()]

    matches = []
    errors = []
    for t, s, y in titles:
        try:
            search = execute(["pdfgrep", "-i", t,  #s.split("_")[0],
                              ZHAO_EVENT_PREDICTION_SURVEY_PATH])
            #if len(search) > 0 and y in " ".join(search):
            if len(search) > 0:
                #if s.split("_")[0] in search or y in search:
                matches.append((s, search, y))
        except subprocess.CalledProcessError:
            errors.append(["pdfgrep", "-i", t,
                           ZHAO_EVENT_PREDICTION_SURVEY_PATH])
    # Searched with title and prefix length 10
    false_positives = ['zhou_multi-task_2021','zilker_designing_2022','li_future_2021','huang_script_2021','chiorrini_exploiting_2022','portolani_uncertainty_2022','di_mauro_activity_2019','lopez_castro_development_2021','khan_predictive_2019','fernandes_impact_2022','ogunbiyi_investigating_2020','vandenabeele_enhancing_2022','huang_time_2014','wu_uncertainty-aware_2021','rogge-solti_prediction_2015','firouzian_real-time_2019','bevacqua_data-driven_2014','solomon_business_2011','klijn_identifying_2020','ogunbiyi_incorporating_2021','gunnarsson_predictive_2019','bolt_process_2014','dubey_bayesian_2022','weinzierl_predictive_2020','wang_interval-based_2021','galdo_seara_approach_2019','lindemann_anomaly_2020','tan_adaptive_2010','langone_interpretable_2020','huang_using_2016','lajevardi_combination_2008','vrignat_failure_2015','hu_time-series_2021','cabanillas_predictive_2014','kaveh_new_2020','liu_multi-channel_2022','bouzar-benlabiod_variational_2019','tariq_proactive_2021','lu_collaborative_2021','cuzzocrea_predictive_2019','diraco_machine_2023','ngaffo_event_2019','liu_constructing_2021','zhang_deep_2017','karakostas_event_2016','rudin_learning_2013','lu_business_2022','tama_leveraging_2022','stevens_quantifying_2022','ronzani_unstructured_2022','francescomarino_clustering-based_2019','verenich_complex_2016','di_francescomarino_genetic_2018','liu_prediction_2018','weinzierl_predictive_2021','metzger_predictive_2017','teinemaa_predictive_2016','maggi_predictive_2014','cekinel_event_2022','zhong_prediction_2019','xue_spatial_2006','fox_using_2012','mcginty_spatial_2009','taymouri_predictive_2020','tax_predictive_2017','rico_business_2021','wu_joint_2022','fullop_real_2012','ceci_completion_2014','bohmer_probability_2018','bai_integrating_2021','wang_integrating_2017','zhang_introducing_2022'

    ]
    false_negatives = ['zhong_prediction_2019', 'matsubara_fast_2012', 'ramakrishnan_beating_2014', 'zhou_tensor_2019', 'susto_machine_2015']
    title_keys = [d[0] for d in matches]
    t = [df_final[df_final["RawSource"] == s] for s, search, y in matches if s not in false_positives]
    df_t = pd.concat(t)
    df_inter_counts = pd.DataFrame(df_t["Area"].value_counts())

    # Author year based search

    matches_auth = []
    errors_auth = []
    for t, s, y in titles:
        try:
            search = execute(["pdfgrep", "-i", s.split("_")[0],
                              ZHAO_EVENT_PREDICTION_SURVEY_PATH])
            if len(search) > 0 and y in " ".join(search):
            #if len(search) > 0:
                # if s.split("_")[0] in search or y in search:
                matches_auth.append((s, search, y))
        except subprocess.CalledProcessError:
            errors_auth.append(["pdfgrep", "-i", t,
                                ZHAO_EVENT_PREDICTION_SURVEY_PATH])
    # Searched with title and prefix length 10
    auth = [df_final[df_final["RawSource"] == s] for s, search, y in matches_auth if
            (s not in [k for k in title_keys])]
    to_add = [df_final[df_final["RawSource"] == s] for s in false_negatives if s not in [m[0] for m in matches] and
              s not in [m[0] for m in matches_auth]]
    df_auth = pd.concat(auth + [df_t] + to_add)
    df_all_inter_counts = pd.DataFrame(df_auth["Area"].value_counts())
    del ax, bar_minus, bar_tilde, bar_total, bas_cat_type, bas_type, bast_cat_type, bast_type, cat_type
    del curr_row, df_classgrouped, df_j, df_melted, e, sec, s, tilde_bar, tim_type, top_bar, v, y

    EP = "EP"
    EPS = "EPS"
    IP = "IP"
    AP = "AP"
    PPM = "PPM"
    PPMEP = "EP/PPM"
    areas = [EP, EPS, IP, AP, PPM, PPMEP]

    methodology_counts = {
        k: df_search.iloc[df_search.index.get_loc(k)] - df_all_inter_counts.iloc[df_all_inter_counts.index.get_loc(k)]
        for k in areas
    }
    eps_search = methodology_counts['EP'] + methodology_counts['EPS'] + methodology_counts['IP'] + methodology_counts['AP'] + methodology_counts['EP/PPM']
    print(f'The PPM search methodology pillar has {methodology_counts['PPM'].values[0]} articles\n')
    print(f'The Event Prediction search methodology pillar has {eps_search.values[0]} articles\n')
    print(
        f'The Event Prediction survey methodology pillar has {(len(df_final) - methodology_counts['PPM'] - eps_search).values[0]} articles\n')
    # PPM is the PPM search methodology pillar
    # Areas EP, EPS, IP, AP, EP/PPM are the EP search methodology pillar
    # The remaining articles are from the recent Zhao survey
    df_final['year'] = df_final['RawSource'].str.extract(r'(\d{4})')
    df_final_output = df_final.drop(columns=["Area"])
    df_final_output.to_csv(PATH + 'final_assessment_classification.csv', index=False)

    healthcare_papers = df_final[df_final["Domain1"] == "Healthcare"]

    df_trends = df_final.reset_index()
    df_trends['year'] = pd.to_datetime(df_trends['year'], format='%Y')
    indices = df_trends.index[df_trends['RawSource'] == 'diraco_machine_2023'].tolist()
    # have to exclude 2023
    df_trends = df_trends.drop(index=indices[0])
    df_melted = pd.melt(df_trends, id_vars=['index', 'year'], value_vars=criterias,
                        var_name='Column', value_name='Value')
    plt.figure(figsize=(12, 8))
    sns.set_theme(style="whitegrid")

    sns.stripplot(x='year', y='Column', hue='Value', data=df_melted, jitter=True, palette="Set2", dodge=True,
                  marker='o')

    plt.title('Timeline of Different Categorical Cell Values')
    plt.xlabel('Area')
    plt.ylabel('Columns')
    plt.legend(title='Value', bbox_to_anchor=(1.05, 1), loc='upper left')

    plt.show()

    df_req_count = df_melted.groupby(['year', 'Column', 'Value']).size().reset_index(name='Count')
    plt.figure(figsize=(16, 10))
    sns.set_theme(style="whitegrid")

    cmap = sns.color_palette([color_minus, color_tilde, color_plus])
    g = sns.catplot(x='year', y='Count', hue='Value', col='Column', data=df_req_count, kind='bar', height=5, aspect=0.7,
                    palette=cmap, col_wrap=3)

    g.set_titles("{col_name}")
    g.set_axis_labels("Year", "Count")
    g.add_legend(title="Value")

    plt.savefig(PATH + "trends.svg")
    plt.close()

    df_melted = pd.melt(df_trends, id_vars=['year', 'index'],
                        value_vars=["Joint RCs", "Multiple Sources", "Suffix Prediction"],
                        var_name='Column', value_name='Value')

    df_req_count = df_melted.groupby(['year', 'Column', 'Value']).size().reset_index(name='Count')
    plt.figure(figsize=(16, 14))
    sns.set_theme(style="whitegrid")

    g = sns.catplot(x='year', y='Count', hue='Value', col='Column', data=df_req_count, kind='bar', height=5, aspect=0.9,
                    palette=cmap, col_wrap=3)

    g.set_titles("{col_name}")
    g.set_axis_labels("Year", "Count")

    for ax in g.axes.flat:  ax.tick_params(axis='x', labelrotation=45)

    plt.savefig(PATH + "trends_selected.svg")

